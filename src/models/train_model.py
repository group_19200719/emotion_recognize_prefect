import json
import os

import emotion_net_train as emnet_train
import mlflow.utils
import mlflow
import numpy as np
import pytorch_lightning as pl
import torch
from dotenv import find_dotenv, load_dotenv
from matplotlib import pyplot as plt
from mlflow import log_artifact
from mlflow.models import infer_signature
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from sklearn.metrics import ConfusionMatrixDisplay, confusion_matrix
from prefect import flow, task


def read_env_variables():
    load_dotenv(find_dotenv())


# @task(log_prints=True)
def confision_matrix_as_picture(y_preds, y_true, class_labels):
    cm = confusion_matrix(y_true, y_preds, normalize="true")
    cm = np.round((cm * 100), 2)

    ConfusionMatrixDisplay(cm, display_labels=class_labels).plot(
        xticks_rotation="vertical"
    )

    if not os.path.exists("outputs"):
        os.makedirs("outputs")

    plt.savefig("outputs/confusion_matrix.png")


# @flow(log_prints=True)
def train_model(path_to_cfg, mlflow_server_uri):
    # по структуре кода ориентировался на офф. документацию
    # PyTorch Lightning
    # https://mlflow.org/docs/latest/python_api/mlflow.pytorch.html

    # считываем параметры для обучения модели
    with open(path_to_cfg, "r") as read_file:
        cfg = json.load(read_file)

    # задаем настройки MlFlow
    mlflow.set_tracking_uri(mlflow_server_uri)
    mlflow.set_experiment(cfg["mlflow_experiment_name"])

    output_folder = "outputs"

    # загружаем датасеты, создаем модель
    cfg, dm, df_train, df_test = emnet_train.get_datamodule(cfg)

    # опр. модель
    model = emnet_train.get_model(cfg)
    
    # если указан run MLFLow то берем модель из него и продолжаем ее обучение
    if cfg["model_run"]:
        model.neural_net = mlflow.pytorch.load_model(cfg["logged_model"])

        if cfg["fc_only"]:
            model.set_trainable_fc_only()
        else:
            model.set_trainable()

    # создадим обратный вызов на раннюю остановку обучения
    early_stop_callback = EarlyStopping(
        monitor="val_f1", patience=cfg["patience"], verbose=False, mode="max"
    )

    # создадим обратный вызов на сохранение чекпоинта модели,
    # по интересующей нас метрике
    # saves top-K checkpoints based on "val_f1" metric
    ckpt_names = "check_point-{epoch:02d}-{val_f1:.2f}"

    monitor_metric = "val_f1"
    checkpoint_callback_val_f1 = ModelCheckpoint(
        save_top_k=5, monitor=monitor_metric, mode="max", filename=ckpt_names
    )

    mlflow.pytorch.autolog(log_models=False)

    with mlflow.start_run():
        current_run_id = mlflow.active_run().info.run_id

        # создаем логгер PyTorch Lightning для MlFlow
        mlf_logger = pl.loggers.MLFlowLogger(
            experiment_name=cfg["mlflow_experiment_name"],
            tracking_uri=mlflow_server_uri,
            run_id=current_run_id,
            log_model=False,
        )

        trainer = pl.Trainer(
            logger=mlf_logger,
            accelerator="gpu",
            callbacks=[early_stop_callback, checkpoint_callback_val_f1],
            max_epochs=cfg["max_epochs"],
            log_every_n_steps=10,
        )

        emnet_train.write_labels_to_txt(cfg["class_labels"], output_folder)

        # обучаем модель
        trainer.fit(model, dm)

        # забираем лучшую модель
        best_checkpoint_path = checkpoint_callback_val_f1.best_model_path
        # модель будет в Pytorch Lightning
        model = model.load_from_checkpoint(best_checkpoint_path)

        best_model_score = checkpoint_callback_val_f1.best_model_score
        mlflow.log_metrics({f"final_model_{monitor_metric}": best_model_score})

        # predict with the model
        y_preds_batchs = trainer.predict(model, dm.val_dataloader())
        y_preds = []
        for y_preds_batch in y_preds_batchs:
            y_preds.extend(y_preds_batch)

        y_true = df_test["emotion_class"].values

        # построим confusion matrix
        confision_matrix_as_picture(y_preds, y_true, cfg["class_labels"])

        log_artifact("outputs/classes_labels.txt", artifact_path="model")
        log_artifact("outputs/confusion_matrix.png")
        log_artifact(path_to_cfg)

        if cfg["opt_lr"]:
            log_artifact("outputs/opt_learning_rate.png")

        del cfg["emotion_num"]
        del cfg["mlflow_experiment_name"]

        mlflow.log_params(cfg)

        # для инференса преобразуем в PyTorch без Lightning'a
        torch.save(model.neural_net, "my_model.pth")
        model = torch.load("my_model.pth")
        os.remove("my_model.pth")
        model.eval()
        model.class_labels = tuple(cfg["class_labels"])
        model.dim = tuple(cfg["model_inp_shape"])

        # inference signature
        model_input = next(iter(dm.val_dataloader()))[0]
        model_output = model(model_input)
        model_input = model_input.detach().cpu().numpy()
        model_output = model_output.detach().cpu().numpy()
        signature = infer_signature(model_input, model_output)

        mlflow.pytorch.log_model(model, artifact_path="model", signature=signature)


# @flow(log_prints=True)
def train_emotion_model():
    # сменим директорию на корневой каталог проекта
    emnet_train.chdir_to_projects_root()

    # считаем переменные в рабочее окружение
    read_env_variables()
    mlflow_server_uri = os.getenv("MLFLOW_SERVER_URI")

    train_model("src/models/cfg/model_cfg.json", mlflow_server_uri)


if __name__ == "__main__":
    train_emotion_model()
