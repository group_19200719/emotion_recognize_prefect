import emotion_net_train as emnet_train
from onnx2torch import convert
import torch

# сменим директорию на корневой каталог проекта
emnet_train.chdir_to_projects_root()
    
# Path to ONNX model
onnx_model_path = 'buffalo_l_ResNet50_WebFace600K_gender_age.onnx'
# You can pass the path to the onnx model to convert it or...
torch_model = convert(onnx_model_path)

torch.save(torch_model, 'buffalo_l_ResNet50_WebFace600K_gender_age.ptz')