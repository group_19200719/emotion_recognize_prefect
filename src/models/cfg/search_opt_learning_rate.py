import json
import os

import emotion_net_train as emnet_train
import mlflow
import numpy as np
import pytorch_lightning as pl
from dotenv import find_dotenv, load_dotenv


def read_env_variables():
    load_dotenv(find_dotenv())


def search_opt_lr(path_to_cfg, mlflow_server_uri):
    # сменим директорию на корневой каталог проекта
    emnet_train.chdir_to_projects_root()

    with open(path_to_cfg, "r") as read_file:
        cfg = json.load(read_file)
        
    # задаем настройки MlFlow
    mlflow.set_tracking_uri(mlflow_server_uri)
    mlflow.set_experiment(cfg["mlflow_experiment_name"])

    # загружаем датасеты
    cfg, dm = emnet_train.get_datamodule(cfg)[:2]

    # TODO: удалить debugging code
    # data_loader = dm.train_dataloader()
    # i = 0
    # for inputs, targets in data_loader:
    #     # print(inputs, inputs.shape)
    #     # print(targets, inputs.shape)
    #     t = targets.detach().numpy()
    #     target_name, target_cnt = np.unique(t, return_counts=True)
    #     target_cnt = target_cnt / cfg["batch_size"]
    #     print(target_name, target_cnt)
    #     i = i + 1
    #     if i > 10:
    #         break

    dataset = dm.train

    # опр. модель
    model = emnet_train.get_model(cfg)
    
    # если указан run MLFLow то берем модель из него и продолжаем ее обучение
    if cfg["model_run"]:
        model.neural_net = mlflow.pytorch.load_model(cfg["logged_model"])

        if cfg["fc_only"]:
            model.set_trainable_fc_only()
        else:
            model.set_trainable()

    trainer = pl.Trainer(accelerator="gpu", log_every_n_steps=10)

    # запустим процесс подбора learning rate
    lr_finder = trainer.tuner.lr_find(model, dm)

    # Plot with
    fig = lr_finder.plot(suggest=True)
    fig.savefig("outputs/opt_learning_rate.png")

    # Pick point based on plot, or get suggestion
    opt_lr = lr_finder.suggestion()
    print(opt_lr)

    cfg["opt_lr"] = True
    cfg["learning_rate"] = opt_lr

    with open("src/models/cfg/model_cfg.json", "w") as write_file:
        json.dump(cfg, write_file, indent=4)


if __name__ == "__main__":
    # считываем параметры для обучения модели
    path_to_cfg = "src/models/cfg/model_cfg.json"

    # считаем переменные в рабочее окружение
    read_env_variables()
    mlflow_server_uri = os.getenv("MLFLOW_SERVER_URI")

    search_opt_lr(path_to_cfg, mlflow_server_uri)
