import json

# параметры отбора видео файлов
selection_by = {
    "path2datset": "data/processed/RAVDESS_face",
    "rel_path2_train": "train.csv",
    "rel_path2_test": "test.csv",
    "img_path_col": "image_path",
    "target_col": "emotion_class",
    "class_labels": [
        "calm",
        "happy",
        "sad",
        "angry",
        "fearful"
    ],
    "debug": False,
    "debug_samples": 0.2,
    "oversampling_flag": False,
    "oversample_rate": 0.75,
    "mlflow_experiment_name": "emotion_classifier",
    "model_name": "buffalo_l_ResNet50_WebFace600K_gender_age",
    "model_weights": "standart_weights",
    "model_inp_shape": (3, 192, 192),
    "model_run": False,
    "logged_model": '',
    "fc_only": False,
    "rescale_size": (192, 192),
    "opt_lr": False,
    "learning_rate": 0.0001,
    "batch_size": 128,
    "num_workers": 12,
    "max_epochs": 20,
    "patience": 20,
}

with open("src/models/cfg/model_cfg.json", "w") as write_file:
    json.dump(selection_by, write_file, indent=4)
