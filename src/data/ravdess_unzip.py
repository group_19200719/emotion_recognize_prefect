# разархивируем датасет RAVDESS с помощью данного скрипта

# Каталог датасета выглядит так:

# RAVDESS
# ├── Video_Song_Actor_01.zip
# ├── Video_Song_Actor_02.zip
# ├── Video_Song_Actor_03.zip
# ├── Video_Song_Actor_04.zip
# ├── Video_Song_Actor_05.zip
# ├── Video_Song_Actor_06.zip
# ├── Video_Song_Actor_07.zip
# ├── Video_Song_Actor_08.zip
# ├── Video_Song_Actor_09.zip
# ├── Video_Song_Actor_10.zip
# ├── Video_Song_Actor_11.zip
# ├── Video_Song_Actor_12.zip
# ├── Video_Song_Actor_13.zip
# ├── Video_Song_Actor_14.zip
# ├── Video_Song_Actor_15.zip
# ├── Video_Song_Actor_16.zip
# ├── Video_Song_Actor_17.zip
# ├── Video_Song_Actor_19.zip
# ├── Video_Song_Actor_20.zip
# ├── Video_Song_Actor_21.zip
# ├── Video_Song_Actor_22.zip
# ├── Video_Song_Actor_23.zip
# ├── Video_Song_Actor_24.zip
# ├── Video_Speech_Actor_01.zip
# ├── Video_Speech_Actor_02.zip
# ├── Video_Speech_Actor_03.zip
# ├── Video_Speech_Actor_04.zip
# ├── Video_Speech_Actor_05.zip
# ├── Video_Speech_Actor_06.zip
# ├── Video_Speech_Actor_07.zip
# ├── Video_Speech_Actor_08.zip
# ├── Video_Speech_Actor_09.zip
# ├── Video_Speech_Actor_10.zip
# ├── Video_Speech_Actor_11.zip
# ├── Video_Speech_Actor_12.zip
# ├── Video_Speech_Actor_13.zip
# ├── Video_Speech_Actor_14.zip
# ├── Video_Speech_Actor_15.zip
# ├── Video_Speech_Actor_16.zip
# ├── Video_Speech_Actor_17.zip
# ├── Video_Speech_Actor_18.zip
# ├── Video_Speech_Actor_19.zip
# ├── Video_Speech_Actor_20.zip
# ├── Video_Speech_Actor_21.zip
# ├── Video_Speech_Actor_22.zip
# ├── Video_Speech_Actor_23.zip
# └── Video_Speech_Actor_24.zip


import glob
import json
import os
import shutil
import zipfile
from pathlib import Path


def ravdess_unzip(ravdess_path, extract_folder, path_to_cfg):
    """Разархивирует dataset RAVDESS в указанный каталог.

    Parameters
    ----------
    ravdess_path : str
        путь к папке содержащей dataset RAVDESS
    extract_folder : str
        путь к папке куда извлекаем данные из dataset'а RAVDESS
    path_to_cfg : str
        путь к конфигурационному файлу, определяющему какие данные мы оставляем
    """
    mask_path = os.path.join(ravdess_path, f"*.zip")
    zip_files = glob.glob(mask_path)

    for zip_file in zip_files:
        with zipfile.ZipFile(zip_file, "r") as zip_ref:
            zip_ref.extractall(extract_folder)

    clean_ravdess(extract_folder, path_to_cfg, extension="*.mp4")


def clean_ravdess(extract_folder, path_to_cfg, extension):
    """Удалить лишнее из извлеченных данных из датасета RAVDESS.

    Parameters
    ----------
    extract_folder : str
        путь к папке содержащей извлеченные данные из dataset RAVDESS
    path_to_cfg : str
        путь к конфигурационному файлу, определяющему какие данные мы оставляем
    extension : str
        расширения файлов внутри каталога с извлеченными данными из dataset'a RAVDESS.
        Например, "*.mp4".
    """
    # считываем параметры отбора видео файлов
    with open(path_to_cfg, "r") as read_file:
        cfg_data = json.load(read_file)

    selection_by = [set(val) for val in cfg_data.values()]

    # отбираем только нужные нам видео
    for path in Path(extract_folder).rglob(extension):
        filename = path.name
        params = filename.split(".")[0].split("-")
        params = [int(param) for param in params]

        select_condition = all(
            [param in cond for param, cond in zip(params, selection_by)]
        )

        if select_condition:
            new_path = os.path.join(extract_folder, filename)
            os.rename(path, new_path)
        else:
            os.remove(path)

    # удаляем пустые папки
    for file in os.scandir(extract_folder):
        if file.is_dir():
            shutil.rmtree(file.path)


if __name__ == "__main__":
    # разархивируем весь датасет
    ravdess_path = "data/raw/RAVDESS"
    extract_folder = "data/interim/RAVDESS_videos"
    path_to_cfg = "src/data/cfg/selection_params_cfg.json"

    ravdess_unzip(ravdess_path, extract_folder, path_to_cfg)
