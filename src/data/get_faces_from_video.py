# Данный скрипт детектирует лица из `mediapipe` и нарезаает crop'ы лиц
# из него в набор лиц актеров изображающих различные эмоции.

import glob
import json
import os

import cv2
import emotion_net_train as emnet_train
import mediapipe as mp
import pandas as pd
import tqdm


def get_emotion_class(image_path):
    """Получить метку класса эмоции из названия картинки датасета.

    Parameters
    ----------
    image_path : str
        путь к картинке из датасета

    Returns
    -------
    int
        метку класса кодирующего эмоцию человека
    """
    image_name = os.path.split(image_path)[-1].split(".")[0]
    emotion_class = int(image_name.split("-")[2])
    return emotion_class


def get_image_path(image_path):
    """Получить путь без корневой папки

    Parameters
    ----------
    image_path : str
        путь к картинке из датасета

    Returns
    -------
    str
        путь к картинке без коневой папки
    """
    return os.sep.join(image_path.split(os.sep)[1:])


def create_csv_annot(datatset_path, image_ext, subset):
    """Создает файл аннотаций в формате csv-таблицы.

    Parameters
    ----------
    datatset_path : str
        путь к датасету из вырезанных кадров из видео
    image_ext : str
        расширение файлов картинок. Например, image_ext='.png'
    subset :
        название подвыборки датасета. Например, 'train', 'val', 'test'
    """
    search_path = os.path.join(datatset_path, subset, f"*{image_ext}")

    image_paths = glob.glob(search_path)
    image_paths.sort()
    # image_paths = [get_image_path(image_path) for image_path in image_paths]
    image_classes = [get_emotion_class(image_path) for image_path in image_paths]

    data = {"image_path": image_paths, "emotion_class": image_classes}

    df = pd.DataFrame.from_dict(data)
    annot_name = f"{subset}.csv"
    annot_path = os.path.join(datatset_path, annot_name)
    df.to_csv(annot_path, index=False)


def get_bbox(img_height, img_width, detection):
    """Переводит нормализованные координаты bbox от face detector'a
    из mediapipe в координаты bbox в пикселях изображения

    Parameters
    ----------
    img_height : int
        высота изображения в пикселях
    img_width : int
        ширина изображения в пикселях
    detection : mediapipe detections struct
        структура хранящая детекции лиц людей от face detector

    Returns
    -------
    (height, width, xmin, ymin)
        координаты bbox в пикселях изображения
    """
    bbox = detection.location_data.relative_bounding_box
    height = int(bbox.height * img_height)
    width = int(bbox.width * img_width)
    xmin = int(bbox.xmin * img_width)
    ymin = int(bbox.ymin * img_height)

    return height, width, xmin, ymin


def runOnVideo(video, max_frames):
    """Генератор кадров из видео. Продолжает генерировать кадры
    пока не достигнуто количество кадров maxFrames.

    Parameters
    ----------
    video : cv2.VideoCapture object
        объект видео прочитанного OpenCV
    max_frames : int
        количество кадров после которого нужно остановить покадровое
        чтение видео.

    """
    read_frames = 0
    while True:
        hasFrame, frame = video.read()
        if not hasFrame:
            break

        yield frame

        read_frames += 1
        if read_frames > max_frames:
            break


# считываем параметры отбора видео файлов
def get_faces_dataset(path_video_folder, video_ext, path_to_cfg, result_dataset_path):
    """Получить датасет из crop'ов лиц из кадров из отобранных ранее видео
    из датасета RAVDESS.

    Parameters
    ----------
    path_video_folder : str
        путь к каталогу с отобранными ранее видео из датасета RAVDESS.
    video_ext : str
        расширение видео с которых будем вырезать кадры
    path_to_cfg : str
        путь к конфигурационному файлу задающему параметры создания датасета
        из crop'ов лиц
    result_dataset_path : str
        путь к папке в которой сохраним полученный датасет из crop'ов лиц
    """
    with open(path_to_cfg, "r") as read_file:
        cfg_data = json.load(read_file)
        cfg_data["subsets"]["train"] = set(cfg_data["subsets"]["train"])
        cfg_data["subsets"]["test"] = set(cfg_data["subsets"]["test"])

    # создаем каталог для хранения датасета из кадров видео
    if not os.path.exists(result_dataset_path):
        os.makedirs(result_dataset_path)

    train_path = os.path.join(result_dataset_path, "train")
    if not os.path.exists(train_path):
        os.makedirs(train_path)

    test_path = os.path.join(result_dataset_path, "test")
    if not os.path.exists(test_path):
        os.makedirs(test_path)

    # MediaPipe solution API
    mp_face_detection = mp.solutions.face_detection

    # Run MediaPipe Face Detection with short range model.
    face_detection = mp_face_detection.FaceDetection(
        min_detection_confidence=0.5, model_selection=1
    )

    search_path = os.path.join(path_video_folder, f"*{video_ext}")

    video_paths = glob.glob(search_path)

    for path_video in video_paths:
        # Extract video properties
        video = cv2.VideoCapture(path_video)  # 'video-input.mp4'
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        frames_per_second = video.get(cv2.CAP_PROP_FPS)
        num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

        # Enumerate the frames of the video
        video_frames_gen = runOnVideo(video, num_frames)
        frame_num = 1
        for frame in tqdm.tqdm(video_frames_gen, total=num_frames):
            if frame_num % cfg_data["every_frame"] == 0:
                video_name = os.path.split(path_video)[-1].split(".")[0]
                actor_num = int(video_name.split("-")[-1])
                img_height, img_width, channels = frame.shape

                # Convert the BGR image to RGB and process it with MediaPipe Face Detection.
                image_rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                results = face_detection.process(image_rgb)

                # сохраняем crop с лицами в датасет
                if results.detections:
                    for idx, detection in enumerate(results.detections):
                        height, width, xmin, ymin = get_bbox(
                            img_height, img_width, detection
                        )

                        frame_name = f"{video_name}_frame_{frame_num}_{idx}.png"
                        frame_path = ""
                        if actor_num in cfg_data["subsets"]["train"]:
                            frame_path = os.path.join(train_path, frame_name)
                        elif actor_num in cfg_data["subsets"]["test"]:
                            frame_path = os.path.join(test_path, frame_name)

                        if frame_path:
                            cv2.imwrite(
                                frame_path,
                                frame[ymin : ymin + height, xmin : xmin + width],
                            )

            frame_num += 1

    face_detection.close()

    create_csv_annot(result_dataset_path, ".png", "train")
    create_csv_annot(result_dataset_path, ".png", "test")


if __name__ == "__main__":
    # сменим директорию на корневой каталог проекта
    emnet_train.chdir_to_projects_root()

    path_video_folder = "data/interim/RAVDESS_videos"
    video_ext = ".mp4"
    path_to_cfg = "src/data/cfg/dataset_cfg.json"
    result_dataset_path = "data/processed/RAVDESS_face"

    get_faces_dataset(path_video_folder, video_ext, path_to_cfg, result_dataset_path)
