import json

# параметры отбора видео файлов
selection_by = {
    "modality": [2],
    "vocal_channel": [1, 2],
    "emotion": list(range(1, 8 + 1)),
    "emotional_intensity": [1, 2],
    "statement": [1, 2],
    "repetition": [1, 2],
    "actor": list(range(1, 24 + 1)),
}

with open("selection_params_cfg.json", "w") as write_file:
    json.dump(selection_by, write_file, indent=4)
