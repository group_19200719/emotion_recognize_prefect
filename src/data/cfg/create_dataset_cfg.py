import json

# параметры отбора видео файлов
selection_by = {
    "every_frame": 5,
    "subsets": {"train": list(range(1, 19 + 1)), "test": list(range(20, 24 + 1))},
}

with open("dataset_cfg.json", "w") as write_file:
    json.dump(selection_by, write_file, indent=4)
