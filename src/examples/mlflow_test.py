import os
from random import randint, random

import mlflow.utils
from dotenv import find_dotenv, load_dotenv
from mlflow import log_artifacts, log_metric, log_param

# загружаем переменные окружения из .env-файла в наше рабочее окружение
load_dotenv(find_dotenv())

mlflow_server_uri = os.getenv("MLFLOW_SERVER_URI")
mlflow.set_tracking_uri(mlflow_server_uri)

if __name__ == "__main__":
    experiment_name = "mlflow_test1"

    mlflow.set_experiment(experiment_name)
    log_param("param1", randint(1, 100))

    log_metric("foo", random())
    log_metric("foo", random() + 1)
    log_metric("foo", random() + 2)

    if not os.path.exists("outputs"):
        os.makedirs("outputs")

    with open("outputs/text.txt", "w") as f:
        f.write("Hello World")

    log_artifacts("outputs")
